<?php

namespace Test\Dao;

use Test\Dao\DatabaseTest;
use App\Dao\Pdo\DbPdo;
use \PDO;

/**
 * Description of DbPdoTest
 *
 * @author maurizio
 */
class DbPdoTest extends DatabaseTest {

  private $settings;

  private function getSettings(){
    $settings = require './config/settings.php';
    return (array) $settings;
  }

    protected function getDataSet()
    {
        return $this->createFlatXmlDataSet(__DIR__ . '/heroesFlatXmlFixture.xml');
    }

    public function testQuery()
    {
        $logger = new \Monolog\Logger('/logs/app.log');
        $this->assertEquals(2, $this->getConnection()->getRowCount('heroes'), "Pre-Condition");
        $DbPdo = new DbPdo($this->getSettings(), $logger);
        $params = array(array('column' => 'id', 'value' => 2, 'dataType' => PDO::PARAM_INT));
        $sql = 'select * from heroes where id=:id';

        $queryRes = $DbPdo->query($sql, $params);

        $this->assertEquals(1, $queryRes->rowCount());
    }

    public function testDbDelete()
    {
        $logger = new \Monolog\Logger('/logs/app.log');

        $this->assertEquals(2, $this->getConnection()->getRowCount('heroes'), "Pre-Condition");

        $DbPdo = new DbPdo($this->getSettings(), $logger);
        $DbPdo->dbDelete('heroes', 2, 'id');

        $this->assertEquals(1, $this->getConnection()->getRowCount('heroes'), "Inserting failed");
    }

    public function testDbInserOrUpdate_Update()
    {
        $logger = new \Monolog\Logger('/logs/app.log');
        $this->assertEquals(2, $this->getConnection()->getRowCount('heroes'), "Pre-Condition");
        $DbPdo = new DbPdo($this->getSettings(), $logger);
        $params = [['column' => 'id', 'value' => 2]];
        $sql = 'select * from heroes where id=:id';

        $queryRow1 = $DbPdo->query($sql, $params)->fetch();

        $this->assertNotNull($queryRow1);

        $newValues = array(array('column' => 'date', 'value' => date('2016-04-24')),
            array('column' => 'name', 'value' => 'Salvador Dali'));

        $DbPdo->dbInsertUpdate('heroes', $newValues, 'id', $queryRow1['id']);

        $queryRow2 = $DbPdo->query($sql, $params)->fetch();

        $this->assertEquals('2016-04-24 00:00:00', $queryRow2['date']);
        $this->assertEquals('Salvador Dali', $queryRow2['name']);
    }

    public function testDbInserOrUpdate_Insert()
    {
        $logger = new \Monolog\Logger('/logs/app.log');
        $sql = 'select * from heroes where id=:id';
        $params = [['column' => 'id', 'value' => 3]];

        $this->assertEquals(2, $this->getConnection()->getRowCount('heroes'), "Pre-Condition");

        $DbPdo = new DbPdo($this->getSettings(), $logger);

        $values = array(array('column' => 'date', 'value' => date('2016-04-24')),
            array('column' => 'name', 'value' => 'Maurizio'));

        $DbPdo->dbInsertUpdate('heroes', $values, 'id');

        $queryRow2 = $DbPdo->query($sql, $params)->fetch();
        $this->assertEquals('2016-04-24 00:00:00', $queryRow2['date']);
        $this->assertEquals('Maurizio', $queryRow2['name']);

        $this->assertEquals(3, $this->getConnection()->getRowCount('heroes'), "Inserting failed");
    }

    public function testDbUpdate()
    {
        $logger = new \Monolog\Logger('/logs/app.log');
        $this->assertEquals(2, $this->getConnection()->getRowCount('heroes'), "Pre-Condition");
        $DbPdo = new DbPdo($this->getSettings(), $logger);
        $params = [['column' => 'id', 'value' => 2]];
        $sql = 'select * from heroes where id=:id';

        $queryRow1 = $DbPdo->query($sql, $params)->fetch();

        $this->assertNotNull($queryRow1);

        $newValues = array(array('column' => 'date', 'value' => date('2016-04-24')),
            array('column' => 'name', 'value' => 'Salvador Dali'));

        $DbPdo->dbUpdate('heroes', $queryRow1['id'], $newValues, 'id');

        $queryRow2 = $DbPdo->query($sql, $params)->fetch();
        $this->assertEquals('2016-04-24 00:00:00', $queryRow2['date']);
        $this->assertEquals('Salvador Dali', $queryRow2['name']);
    }

    public function testDbInsert()
    {
        $logger = new \Monolog\Logger('/logs/app.log');
        $sql = 'select * from heroes where id=:id';
        $params = [['column' => 'id', 'value' => 3]];

        $this->assertEquals(2, $this->getConnection()->getRowCount('heroes'), "Pre-Condition");

        $DbPdo = new DbPdo($this->getSettings(), $logger);

        $values = array(
            array('column' => 'date', 'value' => date('2016-04-24')),
            array('column' => 'name', 'value' => 'Maurizio'));


        $DbPdo->dbInsert('heroes', $values);

        $queryRow2 = $DbPdo->query($sql, $params)->fetch();
        $this->assertEquals('2016-04-24 00:00:00', $queryRow2['date']);
        $this->assertEquals('Maurizio', $queryRow2['name']);

        $this->assertEquals(3, $this->getConnection()->getRowCount('heroes'), "Inserting failed");
    }

    public function testInjects()
    {
        $logger = new \Monolog\Logger('/logs/app.log');
        $dboPdoImpl = new DbPdo($this->getSettings(), $logger);
        //    $dboPdoImpl->logSomething();
        $this->assertNotNull($dboPdoImpl->db);
        $this->assertNotNull($dboPdoImpl->logger);
    }

}
