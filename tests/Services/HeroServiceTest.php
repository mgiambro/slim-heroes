<?php

namespace Test\Service;

use Test\Dao\DatabaseTest;
use App\Dao\Pdo\DbPdo;
use \PDO;
use App\Services\Hero\HeroService;
use App\Domain\Hero\Hero;

/**
 * Description of HeroServiceTest
 *
 * @author maurizio
 */

class HeroServiceTest extends DatabaseTest {

  private $settings;

  private function getSettings(){
    $settings = require './config/settings.php';
    return (array) $settings;
  }

  protected function getDataSet()
  {
      return $this->createFlatXmlDataSet(__DIR__ . '../../Dao/heroesFlatXmlFixture.xml');
  }

  protected function randomString($length = 8)
  {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
     $charactersLength = strlen($characters);
     $randomString = '';
     for ($i = 0; $i < $length; $i++) {
         $randomString .= $characters[rand(0, $charactersLength - 1)];
     }
     return $randomString;
  }

  protected function getDbPdo()
  {
    $logger = new \Monolog\Logger('/logs/app.log');
    $DbPdo = new DbPdo($this->getSettings(), $logger);
    return $DbPdo;
  }

  public function testGet()
  {
    $logger = new \Monolog\Logger('/logs/app.log');
    $this->assertEquals(2, $this->getConnection()->getRowCount('heroes'), "Pre-Condition");

    $heroService = new HeroService($this->getDbPdo());
    $hero = $heroService->get(1);

    $this->assertNotNull($hero);
  }

  public function testCreate()
  {
      $hero = new Hero();
      $hero->name = $this->randomString();
      $hero->date = '2016-04-16';
      $heroService = new HeroService($this->getDbPdo());
      $heroService->create($hero);

      $hero = $heroService->get(3);
      $this->assertNotNull($hero);
  }

  public function testUpdate()
  {
      $this->assertTrue(true);
  }
}
