<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use App\Helpers\ConfigHelper;
require '../vendor/autoload.php';

//require '../src/Helpers/ConfigHelper.php';
ConfigHelper::loadEnvironment();

echo getenv('APP_ENV');

// Instantiate the app
$settings = require '../config/settings.php';
//$container = new \Slim\Container;
$app = new \Slim\App(['settings' => $config]);

// Set up dependencies
require '../src/dependencies.php';
// Register middleware
require '../src/middleware.php';
// Register routes
require '../src/routes.php';


// $app->get('/hello/{name}', function (Request $request, Response $response, array $args) {
//     $name = $args['name'];
//     $response->getBody()->write("Hello, $name");
//
//     return $response;
// });
// echo var_dump($container); exit;
// Run app
$app->run();
