<?php

namespace App\Domain\Hero;

class Hero
{

  public $id;
  public $name;
//  public $date;

  public function __construct()
  {

  }

  public  function getId()
  {
    return $this->id;
  }

  public function setId($id)
  {
    $this->id = $id;
  }

  public function getName()
  {
    return $this->name;
  }

  public function setName($name)
  {
    $this->name = $name;
  }

  public function getDate()
  {
    return $this->date;
  }

  public function setDate($date)
  {
    $this->date = $date;
  }
}
