<?php
namespace App\Services\Hero;

use App\Domain\Hero\Hero;
use App\Dao\DbDaoInterface;
use App\Dao\Pdo\DbPdo;
use \PDO;

class HeroService implements HeroServiceInterface
{

  private $dbPdo;

  public function __construct(DbDaoInterface $dbDao) {
    $this->dbPdo = $dbDao;
  }

  public function test()
  {
    echo 'Hello World!!';
  }

  public function list()
  {
    $sql = 'select * from heroes';
    $heroes = array();
    $queryRes = $this->dbPdo->query($sql);

    if($queryRes->rowCount() > 0){
      $records = $queryRes->fetchAll();
      foreach($records as $record){
        $hero = new Hero();
        $hero->setId($record['id']);
        $hero->setName($record['name']);
    //    $hero->setDate($record['date']);
        array_push($heroes, $hero);
      }
    }
//    echo var_dump($heroes); exit;
      return $heroes;
  }

  public function get($id)
  {
    $params = array(array('column' => 'id', 'value' => $id, 'dataType' => PDO::PARAM_INT));
    $sql = 'select * from heroes where id=:id';

    $queryRes = $this->dbPdo->query($sql, $params);

    if($queryRes->rowCount() === 1){
      $record = $queryRes->fetch();
      $hero = new Hero();
      $hero->setId($record['id']);
      $hero->setName($record['name']);
  //    $hero->setDate($record['date']);
      return $hero;
    }

  }

  public function create(Hero $hero)
  {
    $values = array(
        array('column' => 'date', 'value' => $hero->getDate()),
        array('column' => 'name', 'value' => $hero->getName()));

    $this->dbPdo->dbInsert('heroes', $values);
  }

  public function update(){

  }

}
