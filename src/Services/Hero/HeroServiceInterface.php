<?php

namespace App\Services\Hero;

use App\Domain\Hero\Hero;

interface HeroServiceInterface
{

  public function test();

  public function get($id);

  public function create(Hero $hero);

  public function update();

  public function list();

}
