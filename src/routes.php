<?php

$app->get('/api/hero/get/:id', \HeroController::class . ':getHero');

$app->get('/api/hero/list', \HeroController::class . ':getHero');

$app->post('/api/hero/update/:id', \HeroController::class . ':updateHero');

$app->post('/api/hero/create', \HeroController::class . ':createHero');

$app->delete('/api/hero/delete/:id', \HeroController::class . ':deleteHero');

/* SEARCH */

$app->get('api/heroes/?name=:id', \HeroController::class . ':search');
