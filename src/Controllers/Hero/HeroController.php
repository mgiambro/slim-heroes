<?php
namespace App\Controllers\Hero;

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use App\Services\Hero\HeroServiceInterface;
use App\Services\Hero\HeroService;

class HeroController
{

 protected $heroService;

   // constructor receives container instance
   public function __construct(HeroServiceInterface $heroService) {
      $this->heroService = $heroService;
   }

   public function updateHero($request, $response, $args) {
    $jsonResponse = $response->withHeader('Content-type', 'application/x-www-form-urlencoded');
    $jsonResponse = $jsonResponse->withAddedHeader('Access-Control-Allow-Origin','*' );

  //  echo var_dump(json_decode($request->getBody(),true));
   echo var_dump($request->getParsedBody()['name']);

  //     return $ret;
   }

   public function deleteHero($request, $response, $args) {

   }

    public function createHero($request, $response, $args) {

    }

   public function listHeroes($request, $response, $args) {
    $jsonResponse = $response->withHeader('Content-type', 'application/json');
    $jsonResponse = $jsonResponse->withAddedHeader('Access-Control-Allow-Origin','*' );

    //     $ret = $response->getBody()->write($this->heroService->get(1));
      $ret = $jsonResponse->withJson($this->heroService->list());

    return $ret;
   }

   public function getHero($request, $response, $args) {
    $jsonResponse = $response->withHeader('Content-type', 'application/json');
    $jsonResponse = $jsonResponse->withAddedHeader('Access-Control-Allow-Origin','*' );

    //     $ret = $response->getBody()->write($this->heroService->get(1));
      $ret = $jsonResponse->withJson($this->heroService->list());

    return $ret;
   }

}
