<?php

namespace App\Helpers;

class ConfigHelper
{

  static function loadEnvironment(string $filename = '../.env')
  {
    if (file_exists($filename)){
      $envArr = file($filename, FILE_SKIP_EMPTY_LINES);
      foreach ($envArr as $envVar) {
         putenv($envVar);
      }
    }else{
      echo "The file $filename does not exist. Environment config not loaded.";
    }

  }

}
