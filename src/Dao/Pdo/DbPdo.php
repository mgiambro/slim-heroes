<?php

namespace App\Dao\Pdo;

use \PDO;
use App\Dao\DbDaoInterface;
use Psr\Log\LoggerInterface;

/* NEED TO FIX THE LOGGING AND SETTINGS */

class DbPdo implements DbDaoInterface {

    public $db = null;
    public $logger;

    public function __construct($settings, LoggerInterface $logger = null, PDO $db = null)
    {
  //      echo var_dump($settings); exit;
        if (isset($db)) {
            $this->db = $db;
        } else {
            $dbSettings = $settings['db'];
            $this->db = new PDO($dbSettings['dsn'], $dbSettings['user'], $dbSettings['pass']);
        }

        $this->logger = $logger;
        $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }

    public function logSomething()
    {
        $this->logger->info('New log text ... ');
    }

    public function dbDelete($table, $ref, $fieldref = 'ref')
    {
        $parm = array(array('column' => 'ref', 'value' => $ref));
        $sql = 'DELETE FROM ' . $table . ' WHERE ' . $fieldref . '=:ref limit 1';
        $stmt = $this->db->prepare($sql);
        $this->runStatement($sql, $stmt, $parm, 'Delete Completed');
    }

    public function query($sql, $parms = array())
    {

        if (!$this->validateSQL($sql, $parms)) {
            return false;
        }

        $stmt = $this->db->prepare($sql);
        $ret = $this->runStatement($sql, $stmt, $parms, 'Prepared completed');
        return $ret;
    }

    public function dbUpdate($table, $ref, $fields, $fieldref = 'ref')
    {
        $sql = 'update ' . $table;
        $sql.=' set ';
        $partSql = '';

        foreach ($fields as $field) {
            $partSql.=' ' . $field['column'] . '=:' . $field['column'] . ',';
            $this->logger->info($field['column'] . ': ' . $field['value']);
   //         echo $field['value'];
        }
   //     exit;
        array_push($fields, array('column' => 'passedRef', 'value' => $ref));

        $subPartSql = substr($partSql, 0, -1);
        $sql.=$subPartSql . ' where ' . $fieldref . '=:passedRef limit 1;';
        $this->logger->info('sql: ' . $sql);
        $stmt = $this->db->prepare($sql);

        $this->runStatement($sql, $stmt, $fields, 'Update Completed');
    }

    public function dbInsert($table, $fieldList)
    {
        $sql = 'insert into ' . $table . ' ';
        $fields = '(';
        $values = '(';
        foreach ($fieldList as $field) {
            $this->logger->info('Field: ' . $field['column'] . 'Value: ' . $field['value']);
            $fields.=$field['column'] . ',';
            $values.=':' . $field['column'] . ',';
        }
        $fields = substr($fields, 0, -1) . ')';
        $values = substr($values, 0, -1) . ')';

        $sql.=$fields . ' values ' . $values;

        $stmt = $this->db->prepare($sql);
        $this->runStatement($sql, $stmt, $fieldList, 'Insert Completed');
        return $this->db->lastInsertId();
    }

    public function dbInsertUpdate($table, $fields, $refField = 'ref', $ref = '')
    {
        $ret = '';
        $parms = array(array('column' => ':ref', 'value' => $ref));
        $sql = "select * from $table where $refField=:ref limit 1";
        $stmt = $this->query($sql, $parms);
        if ($stmt) {
            $row = $stmt->fetch();
            $this->logger->info('dbInsertUpdate: ' . $row[$refField]);
            if ($row[$refField] > 0 || $row[$refField] != '') {
                $this->dbUpdate($table, $ref, $fields, $refField);
                $ret = $ref;
            } else {
                $ret = $this->dbInsert($table, $fields);
            }
        }
        return $ret;
    }

    private function validateSQL($sql, $parm)
    {
        if (!$this->usesPreparedStatement($sql)) {
            return false;
        }
        if (!$this->checkParameterCount($sql, $parm)) {
            return false;
        }
        return true;
    }

    private function checkParameterCount($sql, $parm)
    {
        $sqlParamsArray = $this->getSQLParams($sql);
        $parmCount = sizeof($sqlParamsArray);
        if (stripos($sql, "insert into" === 0)) {
            if ($parmCount != sizeof($parm)) {
                $this->logger->info('PDO Parameter count mismatch ' . sizeof($parm) . ' passed, expecting ' . $parmCount);
                return false;
            }
        }
        return true;
    }

    private function usesPreparedStatement($sql)
    {
        if (strrpos($sql, '"' > 0) || strrpos($sql, "'" > 0)) {
            $this->logger->info('PDO Must use prepared statements 1');
            return false;
        }

        $sqlParamsArray = $this->getSQLParams($sql);

        foreach ($sqlParamsArray as $value) {
            if (substr(trim($value), 0, 1) != ':') {
                $this->logger->info('PDO Must use prepared statements 2');
                return false;
            }
        }
        return true;
    }

    private function getSQLParams($sql)
    {
        $where = explode('where', strtolower($sql));

        $sqlParams = array_key_exists(1, $where) ? $where[1] : '';

        $sqlParamsArray = explode('=', $sqlParams);
        array_shift($sqlParamsArray); // Remove elements preceing the '='

        return $sqlParamsArray;
    }

    /*
      private function runStatement($sql, $stmt, $fields, $text)
      {
      if ($stmt->execute($fields)) {
      $this->logger->info('PDO' . $text . $stmt->rowCount() . ' Rows affected');
      return $stmt;
      } else {
      $error = $stmt->errorInfo();
      $this->logger->error('PDO ERROR ' . $error[0] . ' ' . $error[2]);
      $this->logger->info('PDO SQL', $sql);
      return false;
      }
      }
     */

    private function runStatement($sql, $stmt, $fields, $text)
    {
        $bindedStatement = $this->bindStatementValues($stmt, $fields);
        if ($bindedStatement->execute()) {
            $this->logger->info('PDO' . $text . $bindedStatement->rowCount() . ' Rows affected');
            return $bindedStatement;
        } else {
            $error = $bindedStatement->errorInfo();
            $this->logger->error('PDO ERROR ' . $error[0] . ' ' . $error[2]);
            $this->logger->info('PDO SQL', $sql);
            return false;
        }
    }

    private function bindStatementValues(\PDOStatement $stmt, Array $params)
    {
        foreach ($params as $param) {
            $dataType = array_key_exists('dataType', $param) ? $param['dataType'] : PDO::PARAM_STR;  // Default to string if data type not passed in.
            $stmt->bindParam($param['column'], $param['value'], $dataType);
        }
        return $stmt;
    }

}
