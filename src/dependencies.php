<?php

use App\Controllers\Hero\HeroController;
use App\Services\Hero\HeroService;
use App\Dao\Pdo\DbPdo;

// Instatiate the container
$container = $app->getContainer();

// logger
$container['logger'] = function($c) {
    $logger = new \Monolog\Logger('my_logger');
    $file_handler = new \Monolog\Handler\StreamHandler('../logs/app.log');
    $logger->pushHandler($file_handler);
    return $logger;
};

// Settings
$container['db'] = function ($c) {
    $db = $c['settings']['db'];
    $pdo = new PDO('mysql:host=' . $db['host'] . ';dbname=' . $db['dbname'],
        $db['user'], $db['pass']);
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
    return $pdo;
};

// HeroService
$container['heroService'] = function ($container) {
    $logger = new \Monolog\Logger('/logs/app.log');
    $dbPdo = new DbPdo($container['settings'], $logger);
    $heroService = new HeroService($dbPdo);
    return $heroService;
};

// HeroController
$container['HeroController'] = function($container) {
    return new HeroController($container['heroService'] );
};
